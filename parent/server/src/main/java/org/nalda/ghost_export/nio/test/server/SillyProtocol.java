package org.nalda.ghost_export.nio.test.server;

import java.io.IOException;
import java.net.Socket;
import java.util.regex.MatchResult;

import org.nalda.ghost_export.nio.test.common.ProtocolAgent;
import org.nalda.ghost_export.nio.test.common.ProtocolVerbs;
import org.nalda.ghost_export.nio.test.common.ProtocolViolationException;

public class SillyProtocol extends ProtocolAgent implements	ProtocolVerbs {

	@FunctionalInterface
	private interface StepExecutor {
		void executeStep() throws ProtocolViolationException, IOException;
	}

	private int firstOperand, secondOperand;

	public SillyProtocol(Socket clientSocket) throws IOException {
		super(clientSocket);
	}

	private void askForNumbers() throws ProtocolViolationException, IOException {
		write(OPERAND_PROMPT);
		expect("(\\d+)\\s*[,;]\\s*(\\d+)", (MatchResult mr) -> { 
			firstOperand = Integer.parseInt(mr.group(1)); 
			secondOperand = Integer.parseInt(mr.group(2)); 
		});
	}
	
	private void greetings() throws IOException, ProtocolViolationException {
		write(WELCOME);
		expect(WELCOME_RESPONSE);
	}
	
	private void sayBye() throws ProtocolViolationException, IOException {
		write(BYE_BYE);
		expect(BYE_BYE_RESPONSE);
	}
	
	private void sendSum() throws ProtocolViolationException, IOException {
		int sum = firstOperand + secondOperand;
		String message = String.format("Sum of %d and %d is %d", firstOperand, secondOperand, sum);

		write(message);
		expect(SUM_RESPONSE);
	}

	private void step(StepExecutor step) throws InterruptedException, ProtocolViolationException, IOException {
		step.executeStep();
	}

	@Override
	public void performProtocolSteps() throws InterruptedException, ProtocolViolationException, IOException {
		step(this::greetings);
		step(this::askForNumbers);
		step(this::sendSum);
		step(this::sayBye);
	}

}
