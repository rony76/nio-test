package org.nalda.ghost_export.nio.test.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NaifServer {
	private static final int AVAILABLE_CORES = 2;
	private static final int SERVER_PORT = 9876;
	private static final int AVAILABLE_CHANNELS = 2;

	public static void main(String[] args) throws IOException {
		ExecutorService executor = Executors
				.newFixedThreadPool(AVAILABLE_CORES);

		try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT,
				AVAILABLE_CHANNELS)) {

			while (true) {
				Socket socket = serverSocket.accept();

				SillyProtocol protocol = new SillyProtocol(socket);
				executor.execute(protocol);
			}
		}
	}
}
