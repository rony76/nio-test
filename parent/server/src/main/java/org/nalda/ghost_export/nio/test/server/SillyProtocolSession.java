package org.nalda.ghost_export.nio.test.server;

import static java.lang.String.format;

import java.nio.channels.SocketChannel;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nalda.ghost_export.nio.test.common.ProtocolVerbs;
import org.nalda.ghost_export.nio.test.common.ProtocolViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SillyProtocolSession implements ProtocolVerbs {
	@FunctionalInterface
	private interface State {
		void acceptLine(String line) throws ProtocolViolationException;
	}

	protected static enum LineSegmentType {
		Full, Chunk
	}

	protected static void consumeDataPerLine(byte[] data,
			BiConsumer<String, LineSegmentType> consumer) {
		Objects.requireNonNull(data, "Invalid null buffer");
		Objects.requireNonNull(consumer, "Invalid null consumer");

		String string = new String(data);
		int lastReadIndex = 0;
		for (int i = 0; i < string.length(); ++i) {
			char c = string.charAt(i);
			if (c == '\r') {
				consumer.accept(string.substring(lastReadIndex, i), LineSegmentType.Full);
				if (i + 1 < string.length() && string.charAt(i + 1) == '\n') {
					i++;
				}
				lastReadIndex = i + 1;
				continue;
			}

			if (c == '\n') {
				consumer.accept(string.substring(lastReadIndex, i), LineSegmentType.Full);
				lastReadIndex = i + 1;
				continue;
			}
		}

		if (lastReadIndex < string.length()) {
			consumer.accept(string.substring(lastReadIndex), LineSegmentType.Chunk);
		}
	}

	private NioServer server;
	private SocketChannel socketChannel;
	private ExecutorService executorService;
	private int id;
	private State state;

	private static AtomicInteger sessionId = new AtomicInteger();
	private static Logger logger = LoggerFactory
			.getLogger(SillyProtocolSession.class);

	private Optional<String> pendingLineChunk = Optional.empty();

	public SillyProtocolSession(NioServer server, SocketChannel socketChannel,
			ExecutorService executorService) {
		this.server = server;
		this.socketChannel = socketChannel;
		this.executorService = executorService;
		id = sessionId.incrementAndGet();

		logMessage("Created");
	}

	public void readData(byte[] data, int count) {
		byte dataCopy[] = new byte[count];
		System.arraycopy(data, 0, dataCopy, 0, count);

		executorService.execute(() -> {
			readDataPerLine(dataCopy);
		});
	}

	public void start() {
		executorService.execute(() -> {
			state = this::waitForWelcomeResponse;
			send(WELCOME);
		});
	}

	private void expect(String expectedMessage, String readMessage)
			throws ProtocolViolationException {
		if (!expectedMessage.equalsIgnoreCase(readMessage)) {
			throw new ProtocolViolationException(
					"Expecting '%s', received '%s' instead", expectedMessage,
					readMessage);
		}

		logMessage("Received '%s'", readMessage);
	}

	private void readDataPerLine(byte[] data) {
		try {
			consumeDataPerLine(data, this::readLine);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void readLine(String line, LineSegmentType segmentType) {
		if (LineSegmentType.Chunk.equals(segmentType)) {
			// is there a pending buffer?
			if (pendingLineChunk.isPresent()) {
				pendingLineChunk = Optional.of(pendingLineChunk.get() + line);
			} else {
				pendingLineChunk = Optional.of(line);
			}
			return;
		}

		String fullLine = pendingLineChunk.isPresent() ? pendingLineChunk.get() + line : line;
		pendingLineChunk = Optional.empty();

		executorService.execute(() -> {
			try {
				state.acceptLine(fullLine);

			} catch (Exception e) {
				logAndClose(e);
			}
		});
	}

	private void send(String message) {
		try {
			server.send(socketChannel, (message + "\n").getBytes());

		} catch (Exception e) {
			logAndClose(e);
		}
	}

	private void terminated(String line) throws ProtocolViolationException {
		throw new ProtocolViolationException(
				"Not waiting for any input once interaction is terminated");
	}

	private void waitForFinalBye(String line)
			throws ProtocolViolationException {
		expect(BYE_BYE_RESPONSE, line);
		state = this::terminated;
		server.close(socketChannel);
	}

	private void waitForNumbers(String line) throws ProtocolViolationException {
		Pattern pattern = Pattern.compile("^(\\-?\\d+)\\s*,\\s*(\\-?\\d+).*$");
		Matcher matcher = pattern.matcher(line);
		if (!matcher.matches()) {
			throw new ProtocolViolationException(
					"Expected comma separated integer numbers, received '%s' instead",
					line);
		}

		logMessage("Received '%s'", line);

		int a = Integer.parseInt(matcher.group(1));
		int b = Integer.parseInt(matcher.group(2));

		int sum = a + b;

		String message = String.format("Sum of %d and %d is %d", a, b, sum);

		state = this::waitForThanks;
		send(message);
	}

	private void waitForThanks(String line) throws ProtocolViolationException {
		expect(SUM_RESPONSE, line);
		state = this::waitForFinalBye;
		send(BYE_BYE);
	}

	private void waitForWelcomeResponse(String line)
			throws ProtocolViolationException {
		expect(WELCOME_RESPONSE, line);
		state = this::waitForNumbers;
		send(OPERAND_PROMPT);
	}

	protected void logAndClose(Exception e) {
		logMessage(e);
		server.close(socketChannel);
		state = this::terminated;
	}

	protected void logMessage(String message) {
		logger.info("Session " + id + ": " + message);
	}

	protected void logMessage(Throwable t) {
		logger.info("Session " + id + ": exception", t);
	}

	void logMessage(String format, Object... params) {
		logMessage(format(format, params));

	}
}
