package org.nalda.ghost_export.nio.test.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NioServer implements Runnable {

	private static class ChangeRequest {

		private SocketChannel socketChannel;
		private int operations;
		private Operation operation;

		public ChangeRequest(SocketChannel socketChannel, Operation operation,
				int operations) {
			this.socketChannel = socketChannel;
			this.operation = operation;
			this.operations = operations;
		}

		public void performChange(Selector selector, NioServer server) {
			SelectionKey key = socketChannel.keyFor(selector);
			if (key == null) {
				// the other end has already shutdown the channel.
				return;
			}

			switch (operation) {
			case ChangeOps:
				key.interestOps(operations);
				break;

			case Close:
				try {
					server.closeKeyChannel(key, "Normal");
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}

		}
	}
	private enum Operation {
		ChangeOps, Close
	}

	private static class WriteRequest {
		private final ByteBuffer buffer;

		private final CompletableFuture<Void> promise = new CompletableFuture<>();

		public WriteRequest(byte data[]) {
			buffer = ByteBuffer.wrap(data);
		}

		public ByteBuffer getBuffer() {
			return buffer;
		}

		public CompletableFuture<Void> getPromise() {
			return promise;
		}
	}

	private static final int AVAILABLE_CORES = 2;
	private static final int SERVER_PORT = 9876;
	public static void main(String[] args) throws IOException {
		try {
			new Thread(new NioServer()).start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private Selector selector;

	private ServerSocketChannel serverChannel;

	private ByteBuffer readBuffer = ByteBuffer.allocate(256);

	private ExecutorService executorService;

	private static Logger logger = LoggerFactory.getLogger(NioServer.class);

	// A list of ChangeRequest instances
	private BlockingQueue<ChangeRequest> changeRequests = new LinkedBlockingQueue<>();

	// Maps a SocketChannel to a list of ByteBuffer instances
	private Map<SocketChannel, BlockingQueue<WriteRequest>> pendingData = new ConcurrentHashMap<>();

	public NioServer() throws IOException {
		super();
		selector = initSelector();
		executorService = Executors.newFixedThreadPool(AVAILABLE_CORES);
	}

	public void close(SocketChannel socketChannel) {
		try {
			changeRequests.put(new ChangeRequest(socketChannel,
					Operation.Close, 0));
			selector.wakeup();

		} catch (InterruptedException ignore) {
		}
	}

	@Override
	public void run() {
		Thread.currentThread().setName("selector-thread");

		while (true) {
			try {
				// Process any pending changes
				ChangeRequest req;
				while ((req = changeRequests.poll()) != null) {
					req.performChange(selector, this);
				}

				// Wait for an event one of the registered channels
				selector.select();

				// Iterate over the set of keys for which events are available
				Iterator<SelectionKey> selectedKeys;
				selectedKeys = selector.selectedKeys().iterator();
				while (selectedKeys.hasNext()) {
					SelectionKey key = selectedKeys.next();
					selectedKeys.remove();

					if (!key.isValid()) {
						continue;
					}

					// Check what event is available and deal with it
					if (key.isAcceptable()) {
						accept(key);
					} else if (key.isReadable()) {
						read(key);
					} else if (key.isWritable()) {
						write(key);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	public CompletionStage<Void> send(SocketChannel socket, byte[] data) {
		CompletableFuture<Void> promise;
		// And queue the data we want written
		BlockingQueue<WriteRequest> queue = pendingData.get(socket);
		if (queue == null) {
			queue = new LinkedBlockingQueue<>();
			pendingData.put(socket, queue);
		}
		WriteRequest req = new WriteRequest(data);
		boolean delivered = false;
		while (!delivered)
			try {
				queue.put(req);
				delivered = true;
			} catch (InterruptedException ignoreAndRetry) {
			}

		promise = req.getPromise();

		try {
			// Indicate we want the interest ops set changed
			changeRequests.put(new ChangeRequest(socket, Operation.ChangeOps,
					SelectionKey.OP_WRITE));

			// Finally, wake up our selecting thread so it can make the required
			// changes
			selector.wakeup();

		} catch (InterruptedException e) {
			// Possibly leaving?
			e.printStackTrace();
		}

		return promise;
	}

	private void accept(SelectionKey key) throws IOException {
		// For an accept to be pending the channel must be a server socket
		// channel.
		ServerSocketChannel serverSocketChannel;
		serverSocketChannel = (ServerSocketChannel) key.channel();

		// Accept the connection and make it non-blocking
		SocketChannel socketChannel = serverSocketChannel.accept();
		socketChannel.configureBlocking(false);

		// Register the new SocketChannel with our Selector, indicating
		// we'd like to be notified when there's data waiting to be read
		SillyProtocolSession processor = new SillyProtocolSession(this,
				socketChannel, executorService);
		socketChannel.register(selector, 0, processor);
		processor.start();
	}

	private Selector initSelector() throws IOException {
		// Create a new selector
		Selector socketSelector = SelectorProvider.provider().openSelector();

		// Create a new non-blocking server socket channel
		serverChannel = ServerSocketChannel.open();
		serverChannel.configureBlocking(false);

		// Bind the server socket to the specified address and port
		InetSocketAddress isa = new InetSocketAddress(SERVER_PORT);
		serverChannel.socket().bind(isa, 1024);

		// Register the server socket channel, indicating an interest in
		// accepting new connections
		serverChannel.register(socketSelector, SelectionKey.OP_ACCEPT);

		return socketSelector;
	}

	private void read(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();

		// Clear out our read buffer so it's ready for new data
		readBuffer.clear();

		// Attempt to read off the channel
		int numRead;
		try {
			numRead = socketChannel.read(readBuffer);
		} catch (IOException e) {
			// The remote forcibly closed the connection, cancel
			// the selection key and close the channel.
			closeKeyChannel(key, e.getMessage());
			return;
		}

		if (numRead == -1) {
			closeKeyChannel(key, "Closed on other end");
			return;
		}

		// Hand the data off to our worker thread
		SillyProtocolSession processor;
		processor = (SillyProtocolSession) key.attachment();

		processor.readData(readBuffer.array(), numRead);
	}

	private void write(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();

		BlockingQueue<WriteRequest> queue = pendingData.get(socketChannel);

		// Write until there's not more data ...
		WriteRequest req;
		while ((req = queue.peek()) != null) {
			ByteBuffer buffer = req.getBuffer();
			socketChannel.write(buffer);
			if (buffer.remaining() > 0) {
				// ... or the socket's buffer fills up
				break;
			}

			queue.poll();

			req.getPromise().complete(null);
		}

		if (queue.isEmpty()) {
			// We wrote away all data, so we're no longer interested
			// in writing on this socket. Switch back to waiting for
			// data.
			key.interestOps(SelectionKey.OP_READ);
		}
	}

	protected void closeKeyChannel(SelectionKey key, String reason) throws IOException {
		Object attachment = key.attachment();
		if (attachment != null) {
			SillyProtocolSession session = (SillyProtocolSession) attachment;
			session.logMessage("Closing channel due to '%s'", reason);

		} else {
			logger.info("Closing channel, due to '%s'");
		}

		SelectableChannel channel = key.channel();
		pendingData.remove(channel);
		channel.close();
		key.cancel();
	}
}
