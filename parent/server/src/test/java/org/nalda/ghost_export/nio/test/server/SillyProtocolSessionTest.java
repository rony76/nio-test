package org.nalda.ghost_export.nio.test.server;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.function.BiConsumer;

import org.junit.Test;
import org.nalda.ghost_export.nio.test.server.SillyProtocolSession.LineSegmentType;

public class SillyProtocolSessionTest {

	private static final String A_STRING = "hello";
	private static final String ANOTHER_STRING = "world";

	@Test
	public void consumePerLineAcceptAndProducesSingleStringWithCarriageReturn()
			throws Exception {
		BiConsumer<String, LineSegmentType> consumer = createMockConsumer();

		SillyProtocolSession.consumeDataPerLine((A_STRING + "\r").getBytes(),
				consumer);

		verify(consumer).accept(A_STRING, LineSegmentType.Full);
	}

	@Test
	public void consumePerLineAcceptAndProducesSingleStringWithCarriageReturnAndLineFeed()
			throws Exception {
		BiConsumer<String, LineSegmentType> consumer = createMockConsumer();

		SillyProtocolSession.consumeDataPerLine((A_STRING + "\r\n").getBytes(),
				consumer);

		verify(consumer).accept(A_STRING, LineSegmentType.Full);
	}

	@Test
	public void consumePerLineAcceptAndProducesSingleStringWithLineFeed()
			throws Exception {
		BiConsumer<String, LineSegmentType> consumer = createMockConsumer();

		SillyProtocolSession.consumeDataPerLine((A_STRING + "\n").getBytes(),
				consumer);

		verify(consumer).accept(A_STRING, LineSegmentType.Full);
	}

	@Test
	public void consumePerLineAcceptAndProducesTwoStringsWithCarriageReturn()
			throws Exception {
		BiConsumer<String, LineSegmentType> consumer = createMockConsumer();

		SillyProtocolSession.consumeDataPerLine((A_STRING + "\r" + ANOTHER_STRING).getBytes(),
				consumer);

		verify(consumer).accept(A_STRING, LineSegmentType.Full);
		verify(consumer).accept(ANOTHER_STRING, LineSegmentType.Chunk);
	}

	@Test
	public void consumePerLineAcceptAndProducesTwoStringsWithCarriageReturnAndLineFeed()
			throws Exception {
		BiConsumer<String, LineSegmentType> consumer = createMockConsumer();

		SillyProtocolSession.consumeDataPerLine((A_STRING + "\r\n" + ANOTHER_STRING).getBytes(),
				consumer);

		verify(consumer).accept(A_STRING, LineSegmentType.Full);
		verify(consumer).accept(ANOTHER_STRING, LineSegmentType.Chunk);
	}

	@Test
	public void consumePerLineAcceptAndProducesTwoStringsWithLineFeed()
			throws Exception {
		BiConsumer<String, LineSegmentType> consumer = createMockConsumer();

		SillyProtocolSession.consumeDataPerLine((A_STRING + "\n" + ANOTHER_STRING).getBytes(),
				consumer);

		verify(consumer).accept(A_STRING, LineSegmentType.Full);
		verify(consumer).accept(ANOTHER_STRING, LineSegmentType.Chunk);
	}

	@Test(expected = NullPointerException.class)
	public void consumePerLineDoesNotAcceptNullBuffer() {
		SillyProtocolSession.consumeDataPerLine(null, (s, t) -> {
		});
	}

	@Test(expected = NullPointerException.class)
	public void consumePerLineDoesNotAcceptNullConsumer() {
		SillyProtocolSession.consumeDataPerLine(A_STRING.getBytes(), null);
	}

	private BiConsumer<String, LineSegmentType> createMockConsumer() {
		@SuppressWarnings("unchecked")
		BiConsumer<String, LineSegmentType> consumer = mock(BiConsumer.class);
		return consumer;
	}
}
