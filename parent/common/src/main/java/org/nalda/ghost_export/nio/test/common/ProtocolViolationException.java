package org.nalda.ghost_export.nio.test.common;

public class ProtocolViolationException extends Exception {
	private static final long serialVersionUID = 8678831173670178675L;

	public ProtocolViolationException(String message, Object... params) {
		super(String.format(message, params));
	}
}