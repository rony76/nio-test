package org.nalda.ghost_export.nio.test.common;

public interface ProtocolVerbs {
	static final String WELCOME = "Welcome";
	static final String BYE_BYE = "Bye bye";
	static final String OPERAND_PROMPT = "2 integer numbers comma separated, please";

	static final String WELCOME_RESPONSE = "Hi";
	static final String SUM_RESPONSE = "Thanks";
	static final String BYE_BYE_RESPONSE = BYE_BYE;
}
