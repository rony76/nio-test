package org.nalda.ghost_export.nio.test.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ProtocolAgent implements Runnable {
	private BufferedReader reader;
	private PrintWriter writer;

	protected Socket clientSocket;

	public ProtocolAgent(Socket clientSocket) throws IOException {
		this.clientSocket = clientSocket;
		reader = new BufferedReader(new InputStreamReader(
				clientSocket.getInputStream()));
		writer = new PrintWriter(clientSocket.getOutputStream());
	}

	protected void expect(String expectedMessage)
			throws ProtocolViolationException, IOException {
		Objects.nonNull(expectedMessage);
		String readMessage = reader.readLine();
		if (readMessage == null) {
			throw new ProtocolViolationException(
					"Expected '%s' but received null!", expectedMessage);
		}

		if (!(readMessage.trim().toLowerCase().equals(expectedMessage
				.toLowerCase()))) {
			throw new ProtocolViolationException(
					"Expected '%s' but received '%s'", expectedMessage,
					readMessage);
		}
	}

	protected void expect(String expectedPattern, Consumer<MatchResult> consumer)
			throws ProtocolViolationException, IOException {
		Objects.nonNull(expectedPattern);
		String readMessage = reader.readLine();
		Matcher matcher = Pattern.compile(expectedPattern).matcher(readMessage);
		if (!matcher.matches()) {
			throw new ProtocolViolationException(
					"Expected '%s' but received '%s'", expectedPattern,
					readMessage);
		}

		consumer.accept(matcher.toMatchResult());
	}

	protected void safeClose() {
		try {
			reader.close();
		} catch (IOException e) {
		}
		writer.close();
		try {
			clientSocket.close();
		} catch (IOException e) {
		}
	}

	protected void write(String message) {
		writer.println(message);
		writer.flush();
	}

	protected abstract void performProtocolSteps()
			throws InterruptedException, ProtocolViolationException,
			IOException;

	@Override
	public void run() {
		try {
			performProtocolSteps();
	
		} catch (Exception e) {
			System.err.println("Exception on protocol: " + e.getMessage());
	
		} finally {
			safeClose();
		}
	}

}