package org.nalda.ghost_export.nio.test.client;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Test;

public class ServiceClientTest {

	@Test
	public void test() throws InterruptedException, ExecutionException, TimeoutException {
		CompletionStage<Void> f = destinedToFail().thenAccept(i -> {
			System.out.println("Returned " + i);
		}).exceptionally(t -> {
			System.out.println("Failed! " + t.getMessage());
			return null;
		}).thenAccept(v -> {
			System.out.println("Finally...");
		});

		f.toCompletableFuture().get(200, TimeUnit.MILLISECONDS);
	}

	private CompletionStage<Integer> destinedToFail() {
		CompletableFuture<Integer> promise = new CompletableFuture<>();
		new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
				promise.completeExceptionally(new NumberFormatException(
						"Oh my God!"));
			}
		}.start();
		return promise;
	}

}
