package org.nalda.ghost_export.nio.test.client.synch;

import static java.lang.String.format;

import java.io.IOException;
import java.net.Socket;
import java.util.Optional;

import org.nalda.ghost_export.nio.test.common.ProtocolAgent;
import org.nalda.ghost_export.nio.test.common.ProtocolVerbs;
import org.nalda.ghost_export.nio.test.common.ProtocolViolationException;

public class SynchronousProtocolClient extends ProtocolAgent implements
		ProtocolVerbs {

	private final int b;
	private final int a;
	private Optional<Integer> result = Optional.empty();

	public int getResult() {
		return result.orElseThrow(() -> new IllegalStateException());
	}

	public SynchronousProtocolClient(Socket clientSocket, int a, int b)
			throws IOException {
		super(clientSocket);
		this.a = a;
		this.b = b;
	}

	protected void performProtocolSteps() throws ProtocolViolationException, IOException {
		expect(WELCOME);
		write(WELCOME_RESPONSE);

		expect(OPERAND_PROMPT);
		write(String.format("%d,  %d", a, b));

		expect(".*is (\\d+)", m -> {
			result = Optional.of(Integer.parseInt(m.group(1)));
		});
		
		if (result.isPresent() && result.get() != (a + b)) {
			throw new IllegalStateException(format("Something very weird happened here! %d + %d <> %d", 
					a, b, result.get()));
		}
		
		write(SUM_RESPONSE);
		
		expect(BYE_BYE);
		write(BYE_BYE_RESPONSE);
	}
}
