package org.nalda.ghost_export.nio.test.client;

import static java.lang.String.format;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceClient {
	private static AtomicLong seed = new AtomicLong();

	private SillyService service;

	private CountDownLatch latch;
	private long clientNumber;
	private String request;

	private static Logger logger = LoggerFactory.getLogger(ServiceClient.class);

	public ServiceClient(SillyService service, CountDownLatch latch) {
		super();
		this.service = service;
		this.latch = latch;
	}

	public CompletionStage<Void> runOn(Executor executor) {
		CompletableFuture<Void> promise = new CompletableFuture<Void>();

		executor.execute(() -> {
			clientNumber = seed.incrementAndGet();
			Random random = new Random(clientNumber);
			int a = random.nextInt(1000);
			int b = random.nextInt(1000);

			request = format("sum of %d and %d", a, b);
			logMessage("Requesting %s", request);

			service.computeSum(a, b).thenAccept(this::onSuccess)
			.exceptionally(this::onFailure).thenAccept(v -> {
				promise.complete(null);
				latch.countDown();
			});
		});

		return promise;
	}

	private void logMessage(String format, Object... params) {
		logger.info("Client " + clientNumber + ": " + format(format, params));
	}

	private void logMessage(String message, Throwable t) {
		logger.info("Client " + clientNumber + ": " + message, t);
	}

	private Void onFailure(Throwable t) {
		logMessage(format("Exception while processing request %s.",
				request), t);

		return null;
	}

	private void onSuccess(Integer sum) {
		logMessage("Got %d for %s", sum, request);
	}
}
