package org.nalda.ghost_export.nio.test.client;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.nalda.ghost_export.nio.test.client.synch.SynchronousSillyService;

public class Client {
	private static final int AVAILABLE_CORES = 640;
	private static final String STAT_DATA_FILE = "../data/runStat.data";
	private static final String SERVER_HOST = "localhost";
	private static final int SERVER_PORT = 9876;

	public static void main(String[] args) throws InterruptedException,
	IOException {
		int clientCount = 10;

		ExecutorService executor = Executors.newFixedThreadPool(AVAILABLE_CORES);

		SillyService service = new SynchronousSillyService(SERVER_HOST,
				SERVER_PORT);

		while (clientCount <= 640) {
			runBatch(clientCount, executor, service);

			clientCount *= 2;
		}

		executor.shutdown();
	}

	protected static void runBatch(int clientCount, ExecutorService executor,
			SillyService service) throws InterruptedException, IOException,
			FileNotFoundException {
		CountDownLatch latch = new CountDownLatch(clientCount);

		ClientStats stats = new ClientStats(clientCount);

		for (int i = 0; i < clientCount; ++i) {
			ServiceClient client = new ServiceClient(service, latch);
			final int index = i;
			stats.start(index);
			client.runOn(executor).thenAccept(v -> {
				stats.stop(index);
			});
		}

		latch.await(2, TimeUnit.MINUTES);

		stats.stopTracing();

		try (FileOutputStream outputStream = new FileOutputStream(
				STAT_DATA_FILE, true)) {

			stats.printStats(outputStream);
		}
	}
}
