package org.nalda.ghost_export.nio.test.client;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.time.LocalTime;
import java.util.Optional;

public class ClientStats {
	private long[] timestamps;
	private LocalTime traceStart = LocalTime.now();
	private Optional<LocalTime> traceStop = Optional.empty();

	public ClientStats(int size) {
		timestamps = new long[size];
	}

	public void start(int index) {
		timestamps[index] = -(System.currentTimeMillis());
	}

	public void stop(int index) {
		timestamps[index] += System.currentTimeMillis();
	}

	public void stopTracing() {
		traceStop = Optional.of(LocalTime.now());
	}

	public void printStats(OutputStream outputStream) {
		try (PrintWriter printWriter = new PrintWriter(outputStream)) {
			printWriter.printf(
					"# Run started at %s, terminated on %s for %d clients.\n",
					traceStart.toString(),
					traceStop.isPresent() ? traceStop.get() : "unterminated",
					timestamps.length);
			printWriter.printf("# Cnt\tMin\tAvg\tMax\n");

			long min = -1, max = -1, sum = -1;
			int cnt = 0;

			for (int idx = 0; idx < timestamps.length; ++idx) {
				long value = timestamps[idx];
				if (value > 0) {
					if (cnt == 0) {
						min = value;
						max = value;
						sum = value;

					} else {
						if (value < min)
							min = value;
						if (value > max)
							max = value;
						sum += value;

					}

					cnt++;
				}
			}

			if (cnt == 0) {
				printWriter.println("0\t0\t0\t0");

			} else {
				printWriter
						.printf("%d\t%d\t%d\t%d\n", cnt, min, sum / cnt, max);

			}
		}
	}
}
