package org.nalda.ghost_export.nio.test.client.synch;

import java.net.Socket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.nalda.ghost_export.nio.test.client.SillyService;

public class SynchronousSillyService implements SillyService {

	private String serverHost;
	private int serverPort;

	public SynchronousSillyService(String serverHost, int serverPort) {
		this.serverHost = serverHost;
		this.serverPort = serverPort;
	}

	@Override
	public CompletionStage<Integer> computeSum(int a, int b) {
		CompletableFuture<Integer> promise = new CompletableFuture<>();
		try {
			Socket socket = new Socket(serverHost, serverPort);
			SynchronousProtocolClient protocolClient = new SynchronousProtocolClient(socket, a, b);
			protocolClient.run();
			promise.complete(protocolClient.getResult());

		} catch (Exception e) {
			promise.completeExceptionally(e);
		}
		return promise;
	}

}
