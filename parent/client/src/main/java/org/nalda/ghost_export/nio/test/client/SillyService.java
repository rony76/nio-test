package org.nalda.ghost_export.nio.test.client;

import java.util.concurrent.CompletionStage;


public interface SillyService {
	CompletionStage<Integer> computeSum(int a, int b);
}
